import { Triangle } from "react-loader-spinner";
import React from "react";

function Loader({ context }) {
  const loadingMessage =
    context === "login"
      ? "Verifying User for Login..."
      : "Changing Password...";

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        marginTop: "15%",
        alignItems: "center",
        flexDirection: "column",
      }}
    >
      <Triangle
        height="80"
        width="80"
        color="rgb(80, 199, 199)"
        ariaLabel="triangle-loading"
        wrapperStyle={{}}
        wrapperClassName=""
        visible={true}
      />
      <h3 style={{ color: "rgb(80, 199, 199)", fontSize: "1.5rem" }}>
        {loadingMessage}
      </h3>
    </div>
  );
}

export default Loader;
