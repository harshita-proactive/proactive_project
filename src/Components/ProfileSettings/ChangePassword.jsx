import React from "react";
import {
  Button,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  TextField,
  Typography,
} from "@mui/material";
import { useState } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import axios from "axios";
import { useNavigate, useParams } from "react-router-dom";
import Header from "../Common/Header";
import Footer from "../Common/Footer";
import Loader from "../App/Loader";

function ChangePassword({ showheader }) {
  const [password, setPassword] = useState("");
  const [confirmPassword, setconfirmPassword] = useState("");
  const [oldPassword, setoldPassword] = useState("");
  const [showPassword, setShowPassword] = React.useState(false);
  const [loader, setLoader] = useState(false);
  const navigate = useNavigate();

  const loaderTimer = () => {
    setTimeout(() => {
      setLoader(false);
    }, 10000);
  };

  const checkPassword = () => {
    if (password === "") return false;
    else if (confirmPassword === "") return false;
    // If Not same return False.
    else if (password === confirmPassword) {
      return true;
    } else return false;
  };

  const changePassword = () => {
    setLoader(true);
    loaderTimer();
    if (checkPassword()) {
      const payload = {
        old_password: oldPassword,
        password: password,
        confirmed_password: confirmPassword,
      };

      const data = axios
        .put(`/changepassword/`, payload, {
          headers: {
            "Content-Type": "application/json",
          },
        })

        .then((resp) => {
          axios.defaults.headers.common[
            "Authorization"
          ] = `Bearer ${resp.data["access"]}`;
        });

      navigate("/login");
      window.location.reload(false);
      // .catch((err) => {
      //   // throw new Error(err);
      //   toast.error("Enter valid password");
      // });
    } else {
      toast.error("Password Dosen't match");
    }
  };
  return !loader ? (
    <div className="imagechgpass">
      {(showheader === undefined || showheader === null) && <Header />}
      <div className="forget-heading">
        <Typography
          style={{
            color: "#35A29F",
            fontWeight: "600",
            textDecoration: "underline",
          }}
          variant="h4"
        >
          Change Password
        </Typography>
      </div>
      <div className="forget-textfield">
        <TextField
          style={{ marginBottom: "1rem" }}
          id="standard-basic"
          label="Enter new password"
          variant="standard"
          type="password"
          autoComplete="current-password"
          value={oldPassword}
          onChange={(e) => setoldPassword(e.target.value)}
        />
        <TextField
          style={{ marginBottom: "1rem" }}
          id="standard-basic"
          label="Enter new password"
          variant="standard"
          type="password"
          autoComplete="current-password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <TextField
          style={{ marginTop: "1rem" }}
          id="standard-basic"
          label="Enter new confirm password"
          variant="standard"
          type="password"
          autoComplete="current-password"
          value={confirmPassword}
          onChange={(e) => setconfirmPassword(e.target.value)}
        />
        <Button
          style={{ marginTop: "4rem", width: "15rem" }}
          type="submit"
          onClick={() => changePassword()}
          variant="contained"
        >
          Reset Password
        </Button>
      </div>

      {(showheader === undefined || showheader === null) && <Footer />}
    </div>
  ) : (
    <Loader context="change password" />
  );
}

export default ChangePassword;
