// import React, { useState } from "react";
// import Header from "../Common/Header";
// import Footer from "../Common/Footer";
// import Dashboard from "../App/Dashboard";
// import { Button, TextField, Typography } from "@mui/material";

// const ChangeProfile = ({ fullName }) => {
//   // Splitting the full name into first name and last name
//   const defaultFirstName = fullName ? fullName.split(" ")[0] : "";
//   const defaultLastName = fullName ? fullName.split(" ")[1] : "";
//   const [firstName, setFirstName] = useState(defaultFirstName);
//   const [lastName, setLastName] = useState(defaultLastName);
//   const [phoneNumber, setPhoneNumber] = useState("");
//   const [image, setImage] = useState(null);

//   const handlePhoneNumberChange = (event) => {
//     setPhoneNumber(event.target.value);
//   };

//   const handleImageChange = (event) => {
//     const file = event.target.files[0];
//     // You may want to add validation for file type, size, etc.
//     // For simplicity, we assume only image files are uploaded.
//     setImage(URL.createObjectURL(file));
//   };

//   const generateInitials = (firstName, lastName) => {
//     return `${firstName.charAt(0)}${lastName.charAt(0)}`.toUpperCase();
//   };

//   const initials = generateInitials(firstName, lastName);

//   return (
//     <div className="imagechgpass">
//       <Header />
//       <Typography
//         variant="h4"
//         style={{
//           display: "flex",
//           justifyContent: "center",
//           marginTop: "1rem",
//           textDecoration: "underline",
//           fontWeight: "bold",
//         }}
//       >
//         Profile Setting
//       </Typography>
//       <div>
//         <div style={{ display: "flex", flexDirection: "row-reverse" }}>
//           <Button>Cancel</Button>
//           <Button>Edit Profile</Button>
//           <Button>Save</Button>
//         </div>
//         <div
//           style={{
//             display: "flex",
//             justifyContent: "center",
//             marginTop: "3rem",
//           }}
//         >
//           <Typography
//             style={{
//               marginRight: "16rem",
//               marginTop: "3rem",
//               fontWeight: "600",
//             }}
//             variant="h6"
//           >
//             Profile Image
//           </Typography>
//           {image ? (
//             <img
//               src={image}
//               alt="Profile"
//               style={{ width: "150px", height: "150px", borderRadius: "50%" }}
//             />
//           ) : (
//             <div
//               style={{
//                 width: "150px",
//                 height: "150px",
//                 borderRadius: "50%",
//                 backgroundColor: "#e0e0e0",
//                 display: "flex",
//                 justifyContent: "center",
//                 alignItems: "center",
//                 fontSize: "50px",
//                 fontWeight: "bold",
//                 color: "#424242",
//               }}
//             >
//               {initials}
//             </div>
//           )}

//           <input
//             type="file"
//             style={{ marginTop: "4rem", marginLeft: "2rem" }}
//             accept="image/*"
//             onChange={handleImageChange}
//           />
//         </div>
//         <div
//           style={{
//             display: "flex",
//             justifyContent: "center",
//             marginRight: "6rem",
//           }}
//         >
//           <Typography
//             style={{
//               marginRight: "16rem",
//               marginTop: "3rem",
//               fontWeight: "600",
//             }}
//             variant="h6"
//           >
//             Email
//           </Typography>
//           <TextField
//             style={{ marginTop: "2rem" }}
//             id="standard-basic"
//             label="Email"
//             variant="standard"
//           />
//         </div>
//         <div
//           style={{
//             display: "flex",
//             justifyContent: "center",
//             marginRight: "6rem",
//           }}
//         >
//           <Typography
//             style={{
//               marginRight: "16rem",
//               marginTop: "3rem",
//               fontWeight: "600",
//             }}
//             variant="h6"
//           >
//             Name
//           </Typography>
//           <TextField
//             style={{ marginTop: "2rem" }}
//             id="standard-basic"
//             label="Name"
//             variant="standard"
//           />
//         </div>
//         <div
//           style={{
//             marginBottom: "13rem",
//             display: "flex",
//             justifyContent: "center",
//             marginRight: "9rem",
//           }}
//         >
//           <Typography
//             style={{
//               marginRight: "16rem",
//               marginTop: "3rem",
//               fontWeight: "600",
//             }}
//             variant="h6"
//           >
//             Phone No.
//           </Typography>
//           <TextField
//             style={{ marginTop: "2rem" }}
//             id="standard-basic"
//             label="Phone Number"
//             variant="standard"
//           />
//         </div>
//       </div>

//       <Footer />
//     </div>
//   );
// };

// export default ChangeProfile;

import React, { useState, useEffect } from "react";
import Header from "../Common/Header";
import Footer from "../Common/Footer";
import Dashboard from "../App/Dashboard";
import { Button, TextField, Typography } from "@mui/material";

const ChangeProfile = ({ fullName, initialEmail, initialPhoneNumber }) => {
  // State to manage the profile data
  const [profileData, setProfileData] = useState({
    firstName: fullName ? fullName.split(" ")[0] : "",
    lastName: fullName ? fullName.split(" ")[1] : "",
    email: initialEmail,
    phoneNumber: initialPhoneNumber,
    image: null,
  });

  // State to track whether the profile is in edit mode
  const [isEditMode, setIsEditMode] = useState(false);

  // Function to fetch user profile data from the backend
  const fetchProfileData = () => {
    // Replace 'fetchBackendData()' with your actual backend API call to fetch the data
    // For simplicity, I'm assuming it returns an object with keys firstName, lastName, email, phoneNumber, and image
    // const backendData = fetchBackendData();

    // Update the state with the fetched data
    setProfileData((prevData) => ({
      ...prevData,
      //   ...backendData,
    }));
  };

  // Function to save the edited profile data to the backend
  const saveProfileChanges = () => {
    // Replace 'saveToBackend()' with your actual backend API call to save the edited data
    // For simplicity, I'm assuming it returns the updated profile data
    // const updatedData = saveToBackend(profileData);

    // Update the state with the updated data from the backend
    // setProfileData(updatedData);

    // Exit the edit mode
    setIsEditMode(false);
  };

  // Function to cancel the editing and reset the data to the original values from the backend
  const cancelProfileChanges = () => {
    // Reset the state with the original data from the backend
    fetchProfileData();

    // Exit the edit mode
    setIsEditMode(false);
  };

  // Fetch profile data from the backend when the component mounts
  useEffect(() => {
    fetchProfileData();
  }, []);

  const handleImageChange = (event) => {
    const file = event.target.files[0];
    // You may want to add validation for file type, size, etc.
    // For simplicity, we assume only image files are uploaded.
    setProfileData((prevData) => ({
      ...prevData,
      image: URL.createObjectURL(file),
    }));
  };

  const generateInitials = (firstName, lastName) => {
    return `${firstName.charAt(0)}${lastName.charAt(0)}`.toUpperCase();
  };

  const initials = generateInitials(
    profileData.firstName,
    profileData.lastName
  );

  return (
    <div className="imagechgpass">
      <Header />
      <Typography
        variant="h4"
        style={{
          display: "flex",
          justifyContent: "center",
          marginTop: "1rem",
          textDecoration: "underline",
          fontWeight: "600",
          color: "rgb(53, 162, 159)",
        }}
      >
        User Profile
      </Typography>
      <div>
        <div style={{ display: "flex", flexDirection: "row-reverse" }}>
          {isEditMode && (
            <>
              <Button
                style={{ fontSize: "1rem", fontWeight: "bold" }}
                onClick={cancelProfileChanges}
              >
                Cancel
              </Button>
              <Button
                style={{ fontSize: "1rem", fontWeight: "bold" }}
                onClick={saveProfileChanges}
              >
                Save
              </Button>
            </>
          )}
          {!isEditMode && (
            <Button
              style={{ fontSize: "1rem", fontWeight: "bold" }}
              onClick={() => setIsEditMode(true)}
            >
              Edit Profile
            </Button>
          )}
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            marginTop: "3rem",
            // marginRight: "21rem",
          }}
        >
          {/* <Typography
            style={{
              marginRight: "16rem",
              marginTop: "3rem",
              fontWeight: "600",
            }}
            variant="h6"
          >
            Profile Image
          </Typography> */}
          {profileData.image ? (
            <img
              src={profileData.image}
              alt="Profile"
              style={{ width: "150px", height: "150px", borderRadius: "50%" }}
            />
          ) : (
            <div
              style={{
                width: "150px",
                height: "150px",
                borderRadius: "50%",
                backgroundColor: "#e0e0e0",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                fontSize: "50px",
                fontWeight: "bold",
                color: "#424242",
              }}
            >
              {initials}
            </div>
          )}

          {isEditMode && (
            <input
              type="file"
              style={{ marginTop: "4rem", marginLeft: "2rem" }}
              accept="image/*"
              onChange={handleImageChange}
            />
          )}
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            // marginRight: "6rem",
          }}
        >
          <Typography
            style={{
              marginRight: "16rem",
              marginTop: "3rem",
              fontWeight: "600",
            }}
            variant="h6"
          >
            Email
          </Typography>
          <TextField
            style={{ marginTop: "2rem" }}
            id="standard-basic"
            label="Email"
            variant="standard"
            disabled
            value={profileData.email}
          />
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            // marginRight: "6rem",
          }}
        >
          <Typography
            style={{
              marginRight: "16rem",
              marginTop: "3rem",
              fontWeight: "600",
            }}
            variant="h6"
          >
            Name
          </Typography>
          <TextField
            style={{ marginTop: "2rem" }}
            id="standard-basic"
            label="Name"
            variant="standard"
            disabled={!isEditMode}
            value={profileData.firstName}
            onChange={(e) =>
              setProfileData((prevData) => ({
                ...prevData,
                firstName: e.target.value,
              }))
            }
          />
        </div>
        <div
          style={{
            marginBottom: "13rem",
            display: "flex",
            justifyContent: "center",
            // marginRight: "9rem",
          }}
        >
          <Typography
            style={{
              marginRight: "13rem",
              marginTop: "3rem",
              fontWeight: "600",
            }}
            variant="h6"
          >
            Phone No.
          </Typography>
          <TextField
            style={{ marginTop: "2rem" }}
            id="standard-basic"
            label="Phone Number"
            variant="standard"
            disabled={!isEditMode}
            value={profileData.phoneNumber}
            onChange={(e) =>
              setProfileData((prevData) => ({
                ...prevData,
                phoneNumber: e.target.value,
              }))
            }
          />
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default ChangeProfile;
