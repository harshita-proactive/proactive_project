

 import React from 'react';
 import axios from 'axios';

 const Webx = () => {
   const handleWebexLogin = async () => {
     try {
       // Redirect the user to the Django backend for Webex login
     
       const response = await axios.get(`/webex/login/`);
       if (response.data && response.data.url) {
         window.location.href = response.data.url;
       }
     } catch (error) {
       console.error('Error during Webex login:', error);
     }
   };

   return (
     <button onClick={handleWebexLogin}>Login with Webex</button>
   );
 };

 export default Webx;

// import React, { useEffect } from 'react';
// import { useLocation, useNavigate } from 'react-router-dom';

// const Webx = () => {
//   const navigate = useNavigate();
//   const location = useLocation();

//   useEffect(() => {
//     const queryParams = new URLSearchParams(location.search);
//     const redirectUri = queryParams.get('redirect_uri');
    
//     // Check if a redirect URI is provided and perform the redirect
//     if (redirectUri) {
//       navigate(redirectUri);
//     } else {
//       // If no redirect URI is provided, redirect to the default page
//       navigate('/dashboard');
//     }
//   }, [location.search, navigate]);

//   return (
//     <div>
//       {/* You can add a loading spinner or message here */}
//     </div>
//   );
// };

// export default Webx;
