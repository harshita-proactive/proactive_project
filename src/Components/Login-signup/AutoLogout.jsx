// import React, { useEffect, useRef } from "react";
// import { useNavigate } from "react-router-dom";

// const AutoLogout = ({ timeoutInSeconds, loginPath }) => {
//   const navigate = useNavigate();
//   const logoutTimerRef = useRef(null);
//   const activityEventsRef = useRef([
//     "mousedown",
//     "mousemove",
//     "keydown",
//     "scroll",
//     "touchstart",
//   ]);

//   const resetLogoutTimer = () => {
//     if (logoutTimerRef.current) {
//       clearTimeout(logoutTimerRef.current);
//     }
//     logoutTimerRef.current = setTimeout(() => {
//       // Clear token from local storage (adjust this based on your actual implementation)
//       localStorage.clear();

//       // Redirect the user to the login page
//       navigate(loginPath);
//       window.location.reload(false);
//     }, timeoutInSeconds * 1000);
//   };

//   const handleActivity = () => {
//     resetLogoutTimer();
//   };

//   useEffect(() => {
//     // Attach event listeners to track user activity
//     const activityEvents = activityEventsRef.current;
//     activityEvents.forEach((event) => {
//       document.addEventListener(event, handleActivity);
//     });

//     // Initialize the timer on component mount
//     resetLogoutTimer();

//     // Clean up event listeners on component unmount
//     return () => {
//       activityEvents.forEach((event) => {
//         document.removeEventListener(event, handleActivity);
//       });
//       clearTimeout(logoutTimerRef.current);
//     };
//   }, [navigate]);

//   return <div></div>;
// };

// export default AutoLogout;

import React from "react";

function AutoLogout() {
  return <div></div>;
}

export default AutoLogout;
