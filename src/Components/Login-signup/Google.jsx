//  import React from 'react';
//  import { GoogleLogin } from '@react-oauth/google';

//  const Google = () => {
//    const handleSuccess = (credentialResponse) => {
//      console.log(credentialResponse);
//    };

//    const handleError = () => {
//      console.log('Login Failed');
//    };

//    return (
//      <GoogleLogin
//        onSuccess={handleSuccess}
//        onError={handleError}
//      />
//   );
//  };

//  export default Google;


// // import React, { useState, useEffect } from 'react';
// // import { googleLogout, useGoogleLogin } from '@react-oauth/google';
// // import axios from 'axios';
// // import { BrowserRouter as Router, useNavigate } from 'react-router-dom';

// // function Google() {
// //   const navigate = useNavigate();
// //   const [user, setUser] = useState([]);
// //   const [profile, setProfile] = useState([]);

// //   const login = useGoogleLogin({
// //     onSuccess: (codeResponse) => setUser(codeResponse),
// //     onError: (error) => console.log('Login Failed:', error),
// //   });

// //   useEffect(() => {
// //     if (user) {
// //       axios
// //         .get(`https://www.googleapis.com/oauth2/v1/userinfo?access_token=${user.access_token}`, {
// //           headers: {
// //             Authorization: `Bearer ${user.access_token}`,
// //             Accept: 'application/json',
// //           },
// //         })
// //         .then((res) => {
// //           setProfile(res.data);
// //         })
// //         .catch((err) => console.log(err));
// //     }
// //   }, [user]);

// //   // log out function to log the user out of google and set the profile array to null
// //   const logOut = () => {
// //     googleLogout();
// //     setProfile(null);
// //   };

// //   const handleNavigation = () => {
// //     navigate('/dashboard'); // Replace '/new-component' with the desired route path
// //   };

// //   return (
// //     <Router>
// //       <div>
// //         {profile ? (
// //           <div>
// //             <img src={profile.picture} alt="user image" />
// //             <h3>User Logged in</h3>
// //             <p>Name: {profile.name}</p>
// //             <p>Email Address: {profile.email}</p>
// //             <br />
// //             <br />
// //             <button onClick={logOut}>Log out</button>
// //             <button onClick={handleNavigation}>Go to New Component</button>
// //           </div>
// //         ) : (
// //           <button onClick={() => login()}>Sign in with Google 🚀</button>
// //         )}
// //       </div>
// //     </Router>
// //   );
// // }

// // export default Google;



// import React, { useState, useEffect } from 'react';
// import { googleLogout, useGoogleLogin } from '@react-oauth/google';
// import axios from 'axios';
// import GoogleToken from './GoogleToken';

// function Google() {
//     const [ user, setUser ] = useState([]);
//     const [ profile, setProfile ] = useState([]);

//     const login = useGoogleLogin({
//         onSuccess: (codeResponse) => setUser(codeResponse),
//         onError: (error) => console.log('Login Failed:', error)
//     });

//     useEffect(
//         () => {
//             if (user) {
//                 axios
//                     .get(`https://www.googleapis.com/oauth2/v1/userinfo?access_token=${user.access_token}`, {
//                         headers: {
//                             Authorization: `Bearer ${user.access_token}`,
//                             Accept: 'application/json'
//                         }
//                     })
//                     .then((res) => {
//                         setProfile(res.data);
//                     })
//                     .catch((err) => console.log(err));
//             }
//         },
//         [ user ]
//     );

//     // log out function to log the user out of google and set the profile array to null
//     const logOut = () => {
//         googleLogout();
//         setProfile(null);
//     };

//     return (
//         <div>
//             <h2>React Google Login</h2>
//             <br />
//             <br />
//             {profile ? (
//                 <div>
//                     <img src={profile.picture} alt="user image" />
//                     <h3>User Logged in</h3>
//                     <p>Name: {profile.name}</p>
//                     <p>Email Address: {profile.email}</p>
//                     <br />
//                     <br />
//                     <button onClick={logOut}>Log out</button>
//                 </div>
//             ) : (
//                 <button onClick={() => login()}>Sign in with Google 🚀 </button>
//             )}

            
//         </div>
//     );
// }
// export default Google;

 import axios from "axios";
 // import { useNavigate } from "react-router-dom";
 import { useState, useEffect } from "react";
 import { useGoogleLogin } from "@react-oauth/google";
 import 'bootstrap/dist/css/bootstrap.css';
 import {googleLogin } from "./axios";

 function responseGoogle(response) {
  console.log(response);
  googleLogin(response.access_token);
}

 export const Google = () => {
   const clientId = "935237227952-pqkfklui1jb7vico2j797q1p8433gn8t.apps.googleusercontent.com";
 //   const navigate = useNavigate();

   const login = useGoogleLogin({
     clientId: clientId,
     onSuccess: async (res) => {
    
       console.log('success:', res.access_token);

       const user = {
         grant_type: "convert_token",
         client_id: "935237227952-pqkfklui1jb7vico2j797q1p8433gn8t.apps.googleusercontent.com",
         client_secret: "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",
         backend: "google-oauth2",
         token: res.access_token,
       };
       try {
         const { data } = await axios.post(
           `/api-auth/convert-token`,
           user,
           {
             headers: {
               "Content-Type": "application/json",
             },
             withCredentials: true,
           }
         );

         axios.defaults.headers.common['Authorization'] = `Bearer ${data['access_token']}`;
         localStorage.clear();
         localStorage.setItem('access_token', data.access_token);
         localStorage.setItem('refresh_token', data.refresh_token);
          window.location.href = '/';
         // navigate("/dashboard");
       } catch (error) {
         console.error(error);
       }
     },
     onFailure: (err) => {
       console.log('failed:', err);
     },
   });

   return (
     <div className="Auth-form-container">
       <button onClick={login}>Sign in with Google</button>
     </div>
   );
 };

 export default Google;


