import {
  Button,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  TextField,
  Typography,
} from "@mui/material";
import React, { useState } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import axios from "axios";
import { useNavigate, useParams } from "react-router-dom";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import Loader from "../App/Loader";

function ForgetPassword() {
  const [password, setPassword] = useState("");
  const [confirmPassword, setconfirmPassword] = useState("");
  const [showPassword, setShowPassword] = React.useState(false);
  const [loader, setLoader] = useState(false);
  const navigate = useNavigate();
  const url = new URL(window.location.href);
  const token = url.searchParams.get("token");
  const uid = url.searchParams.get("id");

  //   const handleClickShowPassword = () => setShowPassword((show) => !show);
  //   const handleMouseDownPassword = (event: React.MouseEvent<HTMLButtonElement>) => {
  //     event.preventDefault();
  //   };

  const loaderTimer = () => {
    setTimeout(() => {
      setLoader(false);
    }, 10000);
  };

  const checkPassword = () => {
    if (password === "") return false;
    else if (confirmPassword === "") return false;
    // If Not same return False.
    else if (password === confirmPassword) {
      return true;
    } else return false;
  };
  const resetPassword = () => {
    setLoader(true);
    loaderTimer();
    if (checkPassword()) {
      const payload = {
        password: password,
        password2: confirmPassword,
      };

      // Create the POST request
      const data = axios
        .post(
          `http://192.168.11.58:8000/reset-password/${uid}/${token}/`,
          payload,
          {
            headers: {
              "Content-Type": "application/json",
            },
          }
        )
        .then((resp) => {
          // Password reset successful, show a success message
          toast.success("Password reset successful!");

          // Redirect the user to the login page
          navigate("/login"); // Replace "/login" with the actual URL of your login page
        })

        .catch((error) => {
          // Handle any errors that occurred during password reset
          toast.error("Password reset failed. Please try again.");
        });
    } else {
      toast.error("Passwords don't match");
    }
  };
  return !loader ? (
    <div className="image">
      <div className="forget-heading">
        <Typography
          style={{
            color: "#35A29F",
            fontWeight: "600",
            textDecoration: "underline",
          }}
          variant="h4"
        >
          Reset Password
        </Typography>
      </div>
      <div className="forget-textfield">
        <TextField
          style={{ marginBottom: "1rem" }}
          id="standard-basic"
          label="Enter new password"
          variant="standard"
          type="password"
          autoComplete="current-password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <TextField
          style={{ marginTop: "1rem" }}
          id="standard-basic"
          label="Enter new confirm password"
          variant="standard"
          type="password"
          autoComplete="current-password"
          value={confirmPassword}
          onChange={(e) => setconfirmPassword(e.target.value)}
        />

        {/* <OutlinedInput
            id="outlined-adornment-password"
            type={showPassword ? 'text' : 'password'}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                  edge="end"
                >
                  {showPassword ? <VisibilityOff /> : <Visibility />}
                </IconButton>
              </InputAdornment>
            }
            label="Password"
          /> */}
        <Button
          style={{ marginTop: "4rem", width: "15rem" }}
          type="submit"
          onClick={() => resetPassword()}
          variant="contained"
        >
          Reset Password
        </Button>
      </div>
    </div>
  ) : (
    <Loader context="forget password" />
  );
}

export default ForgetPassword;
