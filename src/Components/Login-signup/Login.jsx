import React, { useState, useEffect } from "react";
import { Checkbox, Grid, Link, TextField, Typography } from "@mui/material";
import Button from "@mui/material/Button";
import { Link as RouterLink, useNavigate } from "react-router-dom";
import axios from "axios";
import "../../App.css";

import ReactDOM from "react-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import loginimg from "../../Assets/vector-illustration-technology-concept-protecting-600w-2212243313-transformed-removebg-preview.png";
import Loader from "../App/Loader";

const Login = () => {
  const navigate = useNavigate();
  const [isLogin, setIsLogin] = useState(true);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [firstName, setFirstName] = useState("");
  const [phoneNo, setPhoneNo] = useState("");
  const [message, setMessage] = useState("");
  const [loader, setLoader] = useState(false);
  const url = new URL(window.location.href);
  const token = url.searchParams.get("token");

  const loaderTimer = () => {
    setTimeout(() => {
      setLoader(false);
    }, 5000);
  };

  const verifyEmail = () => {
    setLoader(true);
    loaderTimer();
    // Send the token to the backend for verification
    const data = axios
      .post(`/verify/`, {
        token,
      })
      .then((resp) => {
        setMessage(resp.data);

        navigate("/login"); // Replace '/login' with your actual login page route
        //setLoader(false);
      })

      .catch((error) => {
        console.log("error", error);
        toast.error(error?.response?.data?.token[0]);
        //toast.error(error?.response?.data?.email[0]);
        setMessage(error.response.data);
        //setLoader(false);
        setIsLogin(false);
      });

    // Assuming verification is successful, move to the login page
  };

  useEffect(() => {
    if (token !== null && token !== undefined && token !== "") verifyEmail();
  }, [token]);

  function ValidateEmail(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
      return true;
    }

    return false;
  }

  const handleToggle = () => {
    setIsLogin(!isLogin);
  };

  const submitForm = () => {
    if (ValidateEmail(email)) {
      // const entry = { email: email, password: password };
      // setallEntry([...allentry, entry]);
      // console.log(allentry);

      const user = {
        email: email,
        password: password,
      };

      // Create the POST request
      const data = axios
        .post(`http://192.168.11.58:8000/login/`, user, {
          headers: {
            "Content-Type": "application/json",
          },
        })

        .then((resp) => {
          // Initialize the access & refresh token in local storage
          console.log("resp", resp);
          localStorage.clear();
          localStorage.setItem("access_token", resp.data.token.access);
          localStorage.setItem("refresh_token", resp.data.token.refresh);
          axios.defaults.headers.common[
            "Authorization"
          ] = `Bearer ${resp.data.token["access"]}`;
          setEmail("");
          setPassword("");
          setConfirmPassword("");
          setFirstName("");
          setPhoneNo("");
          navigate("/dashboard");
        })
        .catch((err) => {
          console.log("error", err);
          // throw new Error(err);
          toast.error("Enter valid email and password");
        });

      // Navigate to the "/dashboard" route
    } else {
      toast.error("You have entered an invalid email address!");
    }
  };

  const checkPassword = () => {
    if (password === "") return false;
    else if (confirmPassword === "") return false;
    // If Not same return False.
    else if (password !== confirmPassword) {
      return false;
    } else return true;
  };

  const signUp = () => {
    if (ValidateEmail(email)) {
      if (checkPassword()) {
        const payload = {
          email: email,
          name: firstName,
          phone_number: phoneNo,
          password: password,
          password2: confirmPassword,
        };

        // Create the POST request
        const data = axios
          .post(`http://192.168.11.58:8000/register/`, payload, {
            headers: {
              "Content-Type": "application/json",
            },
          })

          .then((resp) => {
            console.log("resp", resp);
            toast.success("Verification email sent");
            console.log("data", data);
          })

          .catch((error) => {
            console.log("error", error);
            toast.error(error.response.data.message);
          });
      } else {
        toast.error("Password dosen't match");
      }
    } else {
      toast.error("Please enter correct email");
    }
  };

  const sendResetPasswordEmail = async () => {
    if (ValidateEmail(email)) {
      try {
        await axios.post(
          "http://192.168.11.58:8000/send-reset-password-email/",
          {
            email: email,
          }
        );
        toast.success("Reset password email sent!");
      } catch (error) {
        toast.error("Failed to send reset password email");
      }
    } else {
      toast.error("You have entered an invalid email address!");
    }
  };

  return !loader ? (
    <>
      <div className="image">
        <Grid
          container
          xs={12}
          sm={12}
          md={12}
          lg={8}
          xl={8}
          spacing={0}
          className="loginCard"
        >
          <Grid
            item
            xs={12}
            sm={6}
            md={6}
            lg={6}
            xl={6}
            className="loginImageDiv"
          >
            {/* Add your logo and image here */}
            <img className="loginImage" src={loginimg} alt="image" />
            <Typography variant="h6" className="loginText"></Typography>
          </Grid>
          <Grid
            item
            xs={12}
            sm={6}
            md={6}
            lg={6}
            xl={6}
            className="loginInputDiv"
            style={{ marginTop: "4rem", height: "32rem" }}
          >
            <Typography variant="h4" className="loginWelText">
              {isLogin ? "Welcome!" : "Create new account!"}
            </Typography>
            <Grid className="loginInputText">
              <Typography variant="h6" className="loginAcc">
                {isLogin ? "Don't have an account yet?" : ""}
              </Typography>
              <Link className="loginSignUp" onClick={handleToggle}>
                {isLogin ? "Sign Up" : "Sign In"}
              </Link>
            </Grid>
            <Grid
              className={`loginInputBox ${isLogin ? "marginTopLogin" : ""}`}
            >
              {/* Add your input fields here */}
              <TextField
                id="email"
                size="small"
                label="Email"
                className="loginInput1"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />

              {!isLogin && (
                <TextField
                  id="FName"
                  size="small"
                  label="Name"
                  className="signupInput2"
                  value={firstName}
                  onChange={(e) => setFirstName(e.target.value)}
                />
              )}

              {!isLogin && (
                <TextField
                  id="Phnno"
                  size="small"
                  label="Phone No"
                  className="signupInput2"
                  value={phoneNo}
                  onChange={(e) => setPhoneNo(e.target.value)}
                />
              )}
              <TextField
                id="outlined-password-input"
                label="Password"
                size="small"
                type="password"
                autoComplete="current-password"
                className="loginInput2"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />

              {!isLogin && (
                <TextField
                  id="confirm-password-input"
                  label="Confirm Password"
                  size="small"
                  type="password"
                  autoComplete="current-password"
                  className="signupInput2"
                  value={confirmPassword}
                  onChange={(e) => setConfirmPassword(e.target.value)}
                />
              )}
              {/* <PasswordChecklist
                  rules={[
                    "capital",
                    "match",
                    "specialChar",
                    "minLength",
                    "number",
                  ]}
                  minLength={8}
                  value={password}
                  valueAgain={matchPassword}
                /> */}
            </Grid>
            {isLogin && (
              <Grid className="loginCheckStatus">
                <Grid className="loginCheckBox">
                  <Checkbox size="small" className="loginCheckboxStyle" />
                  <p className="loginLogText">Keep me logged in</p>
                </Grid>
                <RouterLink
                  className="loginForgetText"
                  to="#"
                  onClick={sendResetPasswordEmail}
                >
                  Forgot Password
                </RouterLink>
              </Grid>
            )}
            <Button
              variant="outlined"
              className={`loginButton ${isLogin ? "marginTop" : ""}`}
              type="submit"
              disabled={password.length <= 0}
              onClick={() => (isLogin ? submitForm() : signUp())}
            >
              {isLogin ? "Login" : "Sign Up"}
            </Button>
          </Grid>
        </Grid>
      </div>

      {/* <div>
        {allentry.map((curElem) => {
          return (
            <div>
              <p>{curElem.email}</p>
              <p>{curElem.password}</p>
            </div>
          );
        })}
      </div> */}
    </>
  ) : (
    <Loader context="login" />
  );
};

export default Login;
