// // import React, { useEffect, useState } from 'react';
// // import { useParams, useNavigate } from 'react-router-dom'; // Import useHistory
// // import axios from 'axios';

// // const EmailVerification = () => {
// //   const { token } = useParams();
// //   const [message, setMessage] = useState('');
// //   const navigate = useNavigate()
// //    // Get the history object from react-router-dom

// //   useEffect(() => {
// //     const verifyEmail = async () => {
// //       try {
// //         const response = await axios.get(`/verify/?token=${token}`);
// //         setMessage(response.data.detail);
// //         // Assuming verification is successful, move to the login page
// //         navigate('/login'); // Replace '/login' with your actual login page route
// //       } catch (error) {
// //         setMessage(error.response.data.detail);
// //       }
// //     };

// //     verifyEmail();
// //   }, [token, navigate]); // Make sure to include history in the dependencies array

// //   return <div>{message}</div>;
// // };

// // export default EmailVerification;

// import React, { useEffect, useState } from 'react';
// import { useParams, useNavigate } from 'react-router-dom';
// import axios from 'axios';

// const EmailVerification = () => {
//   const { token } = useParams();
//   const [message, setMessage] = useState('');
//   const navigate = useNavigate();

//   useEffect(() => {
//     if (!token) {
//       // Token is missing, handle the error or redirect to an error page
//       setMessage('Token is missing.');
//       // Optionally, you can navigate to an error page here
//       // navigate('/error'); // Replace '/error' with the actual error page route
//       return;
//     }

//     const verifyEmail = async () => {
//       try {
//         // Send the token to the backend for verification
//         const response = await axios.post(`/verify/`, { token });
//         setMessage(response.data.detail);
//         // Assuming verification is successful, move to the login page
//         navigate('/login'); // Replace '/login' with your actual login page route
//       } catch (error) {
//         setMessage(error.response.data.detail);
//       }
//     };

//     verifyEmail();
//   }, [token, navigate]);

//   return <div>{message}</div>;
// };

// export default EmailVerification;

