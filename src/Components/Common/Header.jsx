import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Menu from "@mui/material/Menu";
import Container from "@mui/material/Container";
import Avatar from "@mui/material/Avatar";
import MenuItem from "@mui/material/MenuItem";
import AdbIcon from "@mui/icons-material/Adb";
import PermIdentityIcon from "@mui/icons-material/PermIdentity";
import { useNavigate } from "react-router-dom";
import logo from "../../../src/Assets/Proactive-Logo-Compressed.png";

function Header({ setforgetpassword, forgetpassword }) {
  const navigate = useNavigate();
  const [anchorElUser, setAnchorElUser] = React.useState(null);
  const isLoggedIn = true; // Replace this with the actual logic to check the user's login status

  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseUserMenu = (data) => {
    setAnchorElUser(null);
    if (data === "Logout") {
      logout(); // Call the logout function directly
    } else if (data === "Change Password") {
      if (forgetpassword !== undefined && forgetpassword !== null) {
        setforgetpassword("forgetpassword");
      } else {
        navigate("/change-password");
      }
    } else if (data === "Dashboard") {
      navigate("/dashboard");
    } else if (data === "Profile") {
      navigate("/profile");
    }
  };

  const logout = () => {
    localStorage.clear();
    // you can also use localStorage.removeItem('Token');
    window.location.href = "/login";
  };

  if (!isLoggedIn) {
    // Render null when user is not logged in
    return null;
  }

  return (
    <AppBar position="static">
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <img src={logo} alt="" />

          <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
            <PermIdentityIcon style={{ width: "2rem", height: "2rem" }} />
          </IconButton>

          <Menu
            sx={{ mt: "45px" }}
            id="menu-appbar"
            anchorEl={anchorElUser}
            anchorOrigin={{
              vertical: "top",
              horizontal: "right",
            }}
            keepMounted
            transformOrigin={{
              vertical: "top",
              horizontal: "right",
            }}
            open={Boolean(anchorElUser)}
            onClose={handleCloseUserMenu}
          >
            <MenuItem key={4} onClick={() => handleCloseUserMenu("Dashboard")}>
              <Typography textAlign="center">Dashboard</Typography>
            </MenuItem>
            <MenuItem key={4} onClick={() => handleCloseUserMenu("Profile")}>
              <Typography textAlign="center">Profile</Typography>
            </MenuItem>
            <MenuItem
              key={4}
              onClick={() => handleCloseUserMenu("Change Password")}
            >
              <Typography textAlign="center">Change Password</Typography>
            </MenuItem>
            <MenuItem key={4} onClick={() => handleCloseUserMenu("Logout")}>
              <Typography textAlign="center">Logout</Typography>
            </MenuItem>
          </Menu>
        </Toolbar>
      </Container>
    </AppBar>
  );
}

export default Header;
