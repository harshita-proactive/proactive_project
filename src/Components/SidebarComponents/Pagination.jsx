// import React from "react";
// import KeyboardArrowRightIcon from "@mui/icons-material/KeyboardArrowRight";
// import KeyboardArrowLeftIcon from "@mui/icons-material/KeyboardArrowLeft";

// const Pagination = ({
//   totalItems,
//   itemsPerPage,
//   currentPage,
//   onPageChange,
//   onItemsPerPageChange,
// }) => {
//   const totalPages = Math.ceil(totalItems / itemsPerPage);

//   const handlePrevClick = () => {
//     if (currentPage > 1) {
//       onPageChange(currentPage - 1);
//     }
//   };

//   const handleNextClick = () => {
//     if (currentPage < totalPages) {
//       onPageChange(currentPage + 1);
//     }
//   };

//   const handlePageClick = (pageNumber) => {
//     onPageChange(pageNumber);
//   };

//   const handleItemsPerPageChange = (event) => {
//     const newItemsPerPage = parseInt(event.target.value);
//     onItemsPerPageChange(newItemsPerPage);
//   };

//   const renderPageNumbers = () => {
//     const pageNumbers = [];

//     for (let i = 1; i <= totalPages; i++) {
//       pageNumbers.push(
//         <li key={i} className={currentPage === i ? "active" : ""}>
//           <a href="#" onClick={() => handlePageClick(i)}>
//             {i}
//           </a>
//         </li>
//       );
//     }

//     return pageNumbers;
//   };

//   return (
//     <div className="pagination-container">
//       <div className="pagination-rows">
//         Showing {itemsPerPage * (currentPage - 1) + 1} to{" "}
//         {Math.min(itemsPerPage * currentPage, totalItems)} of {totalItems} rows
//       </div>
//       {/* <div className="pagination-dropdown">
//         Rows per page:
//         <select value={itemsPerPage} onChange={handleItemsPerPageChange}>
//           <option value="5">5 Rows</option>
//           <option value="10">10 Rows</option>
//           <option value="20">20 Rows</option>
//         </select>
//       </div> */}
//       <ul className="pagination modal-5">
//         <li>
//           <KeyboardArrowLeftIcon />
//         </li>
//         {renderPageNumbers()}
//         <li>
//           <KeyboardArrowRightIcon />
//         </li>
//       </ul>
//     </div>
//   );
// };

// export default Pagination;
