import React, { useState, useEffect } from "react";
import { Card, CardContent, Grid } from "@mui/material";

function Home() {
  const [counters, setCounters] = useState([
    { value: 0, target: 12000 },
    { value: 0, target: 5000 }, // Set your desired target value for Card 2
    { value: 0, target: 1000 }, // Set your desired target value for Card 3
  ]);

  useEffect(() => {
    const updateCounters = () => {
      setCounters((prevCounters) =>
        prevCounters.map((counter) => {
          if (counter.value < counter.target) {
            const increment = counter.target / 200;
            return { ...counter, value: Math.ceil(counter.value + increment) };
          }
          return counter;
        })
      );
    };

    const timer = setTimeout(updateCounters, 1);

    return () => clearTimeout(timer);
  }, [counters]);

  return (
    <Grid>
      {/* Your links and other components can go here */}
      <Grid item style={{ display: "flex", marginTop: "2rem" }}>
        {counters.map((counter) => (
          <Card
            key={counter.id}
            sx={{ maxWidth: 345 }}
            style={{
              marginTop: "1rem",
              marginLeft: "4rem",
              width: "21rem",
              background: "#50C7C7",
              borderRadius: "20px",
              height: "11rem",
            }}
          >
            <CardContent>
              <h2
                style={{
                  color: "white",
                  textAlign: "center",
                  marginTop: "2.5rem",
                  fontSize: "3rem",
                }}
              >
                {" "}
                {counter.id}
              </h2>
              <h3
                style={{
                  color: "white",
                  textAlign: "center",
                  marginTop: "2.5rem",
                  fontSize: "3rem",
                }}
              >
                {counter.value}
              </h3>
            </CardContent>
          </Card>
        ))}
      </Grid>
    </Grid>
  );
}

export default Home;
