import React from "react";
import "./index.css";

import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Login from "./Components/Login-signup/Login";
import Dashboard from "./Components/App/Dashboard";
import EmailVerification from "./Components/Login-signup/EmailVerification";
import { GoogleOAuthProvider } from "@react-oauth/google";
import ForgetPassword from "./Components/Login-signup/ForgetPassword";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import AutoLogout from "./Components/Login-signup/AutoLogout";
import ChangePassword from "./Components/ProfileSettings/ChangePassword";
import Loader from "./Components/App/Loader";
import ChangeProfile from "./Components/ProfileSettings/ChangeProfile";

function App() {
  return (
    <>
      <Router>
        <Routes>
          <Route exact path="/login" element={<Login />} />
          {/* <Route exact path="/signup" element={<SignUp />} />  */}
          <Route exact path="/dashboard" element={<Dashboard />} />
          <Route path="/verify-email/:token" component={EmailVerification} />
          <Route exact path="/forget-password" element={<ForgetPassword />} />
          <Route exact path="/change-password" element={<ChangePassword />} />
          <Route exact path="/loader" element={<Loader />} />
          <Route exact path="/profile" element={<ChangeProfile />} />
          <Route exact path="/" element={<Login />} />
        </Routes>

        <AutoLogout timeoutInSeconds={30} loginPath="/login" />
        <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
          theme="light"
        />
      </Router>
    </>
  );
}

export default App;
